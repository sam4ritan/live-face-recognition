import paho.mqtt.client as mqtt
import json

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("lifarec/cammove")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    parsed_json = json.loads(msg.payload)
    if parsed_json is None:
        return

    print("dx" + str(parsed_json[0]))
    print("dy" + str(parsed_json[1]))
    # drehen(dx,dy)
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("172.16.0.1", 1883, 60)
client.publish("lifarec/vote", "[89, 11]")
# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
#client.loop_forever()
