import sys

import numpy as np
import cv2 as cv

minVoteArea = 10
maxVoteArea = 10000000000000


cap = cv.imread(sys.argv[1])

hsv = cv.cvtColor(cap, cv.COLOR_BGR2HSV)

lower_green = np.array([60,50,50])
upper_green = np.array([80,255,255])

lower_red = np.array([0,50,50])
upper_red = np.array([20,255,255])
    
mask_green = cv.inRange(hsv, lower_green, upper_green)
res_green = cv.bitwise_and(cap,cap, mask= mask_green)
mask_red = cv.inRange(hsv, lower_red, upper_red)
res_red = cv.bitwise_and(cap,cap, mask= mask_red)


#cv.namedWindow("img", cv.WINDOW_NORMAL)
#cv.resizeWindow("img", 800, 800)
#cv.imshow("img", mask_green)
#cv.waitKey(10000)



inver = cv.bitwise_not(mask_red)

#Green Output

img = inver
params = cv.SimpleBlobDetector_Params()
params.filterByArea = True
params.minArea = minVoteArea
params.maxArea = maxVoteArea
#params.filterByInertia = True
#params.minInertiaRatio = 0.1
detector = cv.SimpleBlobDetector_create(params)
keypoints = detector.detect(img)
print(keypoints)
print("\n GREEN: \n")
for k in keypoints:
    print(k.pt)
    cv.circle(img, (int(k.pt[0]), int(k.pt[1])), int(20), (255, 0, 0))

# Show keypoints
cv.namedWindow("img", cv.WINDOW_NORMAL)
cv.resizeWindow("img", 800, 800)
cv.imshow("img", img)
cv.waitKey(10000)

#Red Output

inver = cv.bitwise_not(mask_red)

img = inver
params = cv.SimpleBlobDetector_Params()
#params.filterByArea = True
#params.minArea = minVoteArea
#params.maxArea = maxVoteArea
#params.filterByInertia = True
#params.minInertiaRatio = 0.1
detector = cv.SimpleBlobDetector_create(params)
keypoints = detector.detect(img)
print(keypoints)
print("\n RED: \n")
for k in keypoints:
    print(k.pt)
    cv.circle(img, (int(k.pt[0]), int(k.pt[1])), int(200), (255, 0, 0))

# Show keypoints
cv.namedWindow("img", cv.WINDOW_NORMAL)
cv.resizeWindow("img", 800, 800)
cv.imshow("img", img)
cv.waitKey(10000)


