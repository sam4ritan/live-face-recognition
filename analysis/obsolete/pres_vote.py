import sys

import numpy as np
import cv2 as cv

minVoteArea = 10000
maxVoteArea = 10000000000000


cap = cv.imread(sys.argv[1])

hsv = cv.cvtColor(cap, cv.COLOR_BGR2HSV)

lower_green = np.array([60,50,50])
upper_green = np.array([80,255,255])

lower_red = np.array([0,50,50])
upper_red = np.array([20,255,255])
    
mask_green = cv.inRange(hsv, lower_green, upper_green)
res_green = cv.bitwise_and(cap,cap, mask= mask_green)
mask_red = cv.inRange(hsv, lower_red, upper_red)
res_red = cv.bitwise_and(cap,cap, mask= mask_red)

#Green Output

img = mask_green


cv.namedWindow("input", cv.WINDOW_NORMAL)
cv.namedWindow("green_output", cv.WINDOW_NORMAL)
cv.namedWindow("red_output", cv.WINDOW_NORMAL)

cv.resizeWindow("input", 800, 400)
cv.resizeWindow("green_output", 800, 400)
cv.resizeWindow("red_output", 800, 400)

cv.imshow("input", cap)
#cv.waitKey(8000)
cv.imshow("green_output", mask_green)
#cv.waitKey(8000)
cv.imshow("red_output", mask_red)
#cv.waitKey(8000)
cv.waitKey(20000)
#cv.waitKey()
