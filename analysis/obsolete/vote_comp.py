#import sys

#import numpy as np
#import cv2 as cv

#minVoteArea = 10000
#maxVoteArea = 10000000000000

def vote(cap)
#cap = cv.imread(sys.argv[1])

hsv = cv.cvtColor(cap, cv.COLOR_BGR2HSV)

lower_green = np.array([60,50,50])
upper_green = np.array([80,255,255])

#lower_yellow = np.array([30,50,50])
#upper_yellow = np.array([50,255,255])

lower_red = np.array([0,50,50])
upper_red = np.array([20,255,255])
    
mask_green = cv.inRange(hsv, lower_green, upper_green)
res_green = cv.bitwise_and(cap,cap, mask= mask_green)
#mask_yellow = cv.inRange(hsv, lower_yellow, upper_yellow)
#res_yellow = cv.bitwise_and(cap,cap, mask= mask_yellow)
mask_red = cv.inRange(hsv, lower_red, upper_red)
res_red = cv.bitwise_and(cap,cap, mask= mask_red)

T = 100
count_red = 0
count_green = 0
count_yellow = 0
h = mask_green.shape[0]
w = mask_green.shape[1]

for y in range(0, h):
    for x in range(0, w):
#        if mask_yellow[y, x] > T:
#            count_yellow = count_yellow + 1
        if mask_green[y, x] > T:
            count_green = count_green + 1
        if mask_red[y, x] > T:
            count_red = count_red + 1
#print("Yellow: " + str(count_yellow))
#print("Green: " + str(count_green))
#print("Red: " + str(count_red))

return count_green, count_red







