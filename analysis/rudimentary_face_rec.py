#Python Analysis Software
import sys
import json
import subprocess
import time
import cv2 as cv
import numpy as np
import paho.mqtt.client as mqtt
import vote_blob_lib

face_cascade = cv.CascadeClassifier('haarcascade_frontalface_default.xml')
broker_url = "172.16.0.1"
camera_server = "172.16.3.34"
image_location = "/media/images"
face_recognition_topic = "lifarec/cammove"
vote_topic = "lifarec/vote"

def face_positions(img):
    return face_cascade.detectMultiScale(img)

def face_position(faces):
    return max(faces, key=(lambda t: (lambda x, y, w, h: w * h)(*t))) if faces != () else None

def center(x, y, w, h):
    return (x + w / 2, y + h / 2, )

def movement_command(position, img_center, deadzones):
    (dx0, dy0), (dx1, dy1) = deadzones
    if position is not None:
        x, y = center(*position)
        deltax, deltay = img_center[0] - x, img_center[1] - y
        return 0 if dx0 <= x < dx1  else deltax, 0 if dy0 <= y < dy1 else deltay
    else:
        return None

def centered_deadzone(img, dx, dy):
    x, y = image_center(img)
    w0, h0 = img.shape[:2]
    w, h = int(w0 * dx), int(h0 * dy)
    return ((x - w, y - h), (x + w, y + h))

def image_center(img):
    h, w = img.shape[:2]
    return (w / 2, h / 2)

def tuple_add(l, r):
    return (l[0] + r[0], l[1] + r[1])

def command_from_image(img):
    faces = face_positions(img)
    face = face_position(faces)
    img_center = image_center(img)
    deadzone = centered_deadzone(img, 0.05, 0.05)
    command = movement_command(face, img_center, deadzone)
    for f in faces:
        x, y, w, h = f
        color = (50, 50, 50) if f is not face else (255, 0, 0)
        cv.rectangle(img, (x, y), (x + w, y + h), color, 2)
    if face is not None:
        cv.circle(img, center(*face), 4, (255, 255, 0))
    cv.circle(img, img_center, 4, (0, 255, 255))
    cv.rectangle(img, deadzone[0], deadzone[1], (0, 255, 0))
    if face is not None and command is not None:
        cv.arrowedLine(img, center(*face), tuple_add(center(*face), command), (0, 0, 255))
    cv.imshow("img", img)
    cv.waitKey(1)
    return command

video = cv.VideoCapture("tcp://172.16.3.34:3333")
#time.sleep(1)

def get_next_frame():
    s, img = video.read()
    if not s or img is None:
        print("Failed to open image")
    else:
        return img

#snap_feed = cv.VideoCapture(0)
wascalled = False
def get_snapshot():
    global wascalled
    #subprocess.call("scp -i {id} {user}@{server}:{dir}/$(ssh -i {id} {user}@{server} 'ls -t {dir} | head -1') curr_img".format(server=camera_server, dir=image_location, user='pi', id='transfer_key'), shell=True)
    #return cv.imread("curr_img", cv.IMREAD_COLOR)
#    return snap_feed.read()[1]
    im = cv.imread(sys.argv[3 if wascalled else 2])
    wascalled = True
    return im


def run_face_recongition():
    client = mqtt.Client()
    client.connect(broker_url)
    try:
        while(True):
            frame = get_next_frame()
            command = command_from_image(frame)
            client.publish(face_recognition_topic, json.dumps(command))
            print(command)
    finally:
        time.sleep(4)
        client.disconnect()

def run_voting():
    client = mqtt.Client()
    client.connect(broker_url)
    cv.namedWindow("img", cv.WINDOW_NORMAL)
    cv.resizeWindow("img", 600, 600)
    try:
        initial = get_snapshot()
        base_green, base_red = vote_blob_lib.vote_blob(initial)
        print("base: g {} r {}".format(len(base_green), len(base_red)))
        total = get_snapshot()
        total_green, total_red = vote_blob_lib.vote_blob(total)
        print("total: g {} r {}".format(len(total_green), len(total_red)))
        client.publish(vote_topic, json.dumps({"red": len(total_red), "green": len(total_green)}))
        #delta_green = max(len(total_green) - len(base_green), 0)
        #delta_red = max(len(total_red) - len(base_red), 0)
        #total = max(delta_red + delta_green, 1)
        #ratio_green = delta_green / total
        #ratio_red = delta_red / total
        #print(ratio_green, ratio_red)
        #client.publish(vote_topic, json.dumps({
        #    "red": int(ratio_red * 100),
        #    "green": int(ratio_green * 100)}))
    finally:
        time.sleep(4)
        client.disconnect()

if __name__ == '__main__':
    if sys.argv[1] == 'face':
        run_face_recongition()
    elif sys.argv[1] == 'vote':
        run_voting()
    else:
        print("Error: unknown command")
